class CreateVehiculos < ActiveRecord::Migration[5.0]
  def change
    create_table :vehiculos do |t|
      t.string :tipo
      t.string :llantas

      t.timestamps
    end
  end
end
