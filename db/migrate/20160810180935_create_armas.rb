class CreateArmas < ActiveRecord::Migration[5.0]
  def change
    create_table :armas do |t|
      t.string :tipo
      t.decimal :calibte

      t.timestamps
    end
  end
end
