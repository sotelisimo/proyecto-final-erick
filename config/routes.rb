Rails.application.routes.draw do
 devise_for :users, controllers: {session: 'users/sessions'}
    devise_for :admins, controllers: {session: 'admins/sessions'}
    
    
  resources :vehiculos
  resources :armas
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
    
    root'armas#index'
end
