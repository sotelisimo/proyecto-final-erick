json.extract! vehiculo, :id, :tipo, :llantas, :created_at, :updated_at
json.url vehiculo_url(vehiculo, format: :json)