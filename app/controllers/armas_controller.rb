class ArmasController < ApplicationController
    load_and_authorize_resource param_method: :my_sanitizer
    load_and_authorize_resource :through => :current_user
    
  before_action :set_arma, only: [:show, :edit, :update, :destroy]

  # GET /armas
  # GET /armas.json
  def index
      @armas = Arma.all.page(params[:page]).per(5)
  end

  # GET /armas/1
  # GET /armas/1.json
  def show
  end

  # GET /armas/new
  def new
    @arma = Arma.new
  end

  # GET /armas/1/edit
  def edit
  end

  # POST /armas
  # POST /armas.json
  def create
    @arma = Arma.new(arma_params)

    respond_to do |format|
      if @arma.save
        format.html { redirect_to @arma, notice: 'Arma was successfully created.' }
        format.json { render :show, status: :created, location: @arma }
      else
        format.html { render :new }
        format.json { render json: @arma.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /armas/1
  # PATCH/PUT /armas/1.json
  def update
    respond_to do |format|
      if @arma.update(arma_params)
        format.html { redirect_to @arma, notice: 'Arma was successfully updated.' }
        format.json { render :show, status: :ok, location: @arma }
      else
        format.html { render :edit }
        format.json { render json: @arma.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /armas/1
  # DELETE /armas/1.json
  def destroy
    @arma.destroy
    respond_to do |format|
      format.html { redirect_to armas_url, notice: 'Arma was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_arma
      @arma = Arma.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def arma_params
      params.require(:arma).permit(:tipo, :calibte)
    end
    
     private
      def my_sanitizer
          params.require(:arma).permit(:tipo, :calibte)
  end
    
end
